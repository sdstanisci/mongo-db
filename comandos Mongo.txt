Para subir o mongo precisa de dois terminais 
O 1 eh para o servico ficar rodando com o comando "mongod --dbpath caminho de onde foram salvos os banco de dados do mongo"
O 2 eh para rodar as linhas de comandos, comando "mongo"

Para importar arquivos Json para uma tabela do mongo pode usar o comando 
mongoimport -c alunos --jsonArray < alunos.json

===========================================================================================

Exemplo de AND

db.alunos.find({ 
	"habilidades.nome":"inglês",
	"nome":"Julio" 
}).pretty()


Exemplo de OR
db.alunos.find( 
	{ $or : 
		[ 
			{"curso.nome":"Medicina"}, 
			{"curso.nome":"Moda"} 
		], 
		"nome":"Daniela" 
}).pretty()
		
Exemplo de IN
db.alunos.find({
	"curso.nome" : { $in : 
		[ 
			"Medicina",
			"Moda"
		]
	}
})

Exemplo INSERT
db.alunos.insert({
	nome: "Fernando",
	data_nascimento: new Date(1994, 03, 26),
	notas: [10, 4.5, 7],
	curso: {
		nome: "Sistemas de Informação"
	}
})	

Exemplo de FIND para somente a primeira linha que for encontrada, o padrão eh sempre false no "multi"
db.alunos.find({ "curso.nome": "Sistemas de Informação" })

Exemplo de UPDATE
db.alunos.update(
	{"curso.nome": "Sistemas de Informação"},
	{
		$set: { "curso.nome": "Analise de Sistemas"}
	}
)

Exemplo de UPDATE para todas as linhas que forem encontradas 
db.alunos.update(
	{"curso.nome": "Sistemas de Informação"},
	{
		$set: { "curso.nome": "Analise de Sistemas"}
	},
	{multi:true}
)

Exemplo de UPDATE para alterar um array completo
db.alunos.update(
	{
		"_id": ObjectId("5a590dbbd8efe479c24568d7")
	},
	
	{
		$set: {	
			"notas": [ 10, 10, 10] 
			}
	},
	
	{
		multi:true
	}
)

Exemplo de UPDATE para incluir um valor dentro de array
db.alunos.update(
	{
		"_id": ObjectId("5a590dbbd8efe479c24568d7")
	},
	
	{
		$push: {	
			notas: 8.5
			}
	},
	
	{
		multi:true
	}
)	

Exemplo de UPDATE varios elementos no array
db.alunos.update(
	{
		"_id": ObjectId("5a590dbbd8efe479c24568d7")
	},
	
	{
		$push: {	
			notas: {$each: [8.5, 3]}
			}
	},
	
	{
		multi:true
	}
)	
	
Exemplo para consulta com nota maior que 5 
db.alunos.find(
	{ 
		notas: {
				$gt:5
		} 
	}
)
	
db.alunos.insert(
	{
		"nome":"André",
		"data_nascimento": new Date(1981, 01, 11),
		"curso":{
					"nome": "História"
		},
		notas: [7, 5, 9, 4.5]
	}
)


Exemplo para buscar apenas um elemento no banco
db.alunos.findOne(
	{ 
		notas: {
				$gt:5
		} 
	}
)

Exemplo para buscar de forma crescente
db.alunos.find().sort(
	{ 
		"nome":1 
	}
).pretty()

Exemplo para buscar de forma crescente com limitador de elementos 
db.alunos.find().sort(
	{ 
		"nome":1 
	}
).pretty()


Exemplo de registro com coordenadas
db.alunos.update(
	{ "_id" : ObjectId("5a5c9333387c2d1ac0a22a8e") },
	{ 
		$set: { 
			localizacao: {
				endereco: "Rua Vergueiro, 3185",
				cidade: "São paulo",
				pais: "Brasil",
				coordinates: "[-23.588213, -46.632356]",
				type: "Point"
				
			}
		}
	}
)

Exemplo de busca de aproximidade 

db.alunos.aggregate([
{
    $geoNear : {
        near : {
            coordinates: [-23.5640265, -46.6527128],
            type : "Point"
        },
        distanceField : "distancia.calculada",
        spherical : true
    }
}
])

db.alunos.createIndex({
    localizacao : "2dsphere"
})

-- com o skipe o mongo despreza o primeiro registro e conta ate o num 4 que eh maximo

db.alunos.aggregate([
{
    $geoNear : {
        near : {
            coordinates: [-23.5640265, -46.6527128],
            type : "Point"
        },
        distanceField : "distancia.calculada",
        spherical : true,
        num : 4
    }
},
{ $skip :1 }
])